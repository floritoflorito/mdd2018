void setup() {
  size(800,800);
  background(150,180,227);
}

void draw() {
  background(150,180,227);
  face(mouseX,mouseY);
}

void face(float x, float y) {
  eye(x-110,y);
  eye(x,y);
  eye(x+110,y);
  mouth(x,y+150,100);
}

void mouth(float x, float y, float wid) {
  strokeWeight(30);
  stroke(255,0,0);
  line(x-wid/2,y,x+wid/2,y);
}

void eye(float x, float y) {
  strokeWeight(1);
  stroke(0);
  fill(255);
  ellipse(x,y,100,150);
  noStroke();
  fill(0);
  ellipse(x+5,y+5,80,130);
  fill(255);
  ellipse(x+20,y-20,20,20);
}
