boolean backgroundIsBlack = true;

void setup() {
  size(500, 500);
}

void draw() {
  if (backgroundIsBlack==true) {
    background(0);
  } else {
    background(255);
  }
  if (mouseX>100 && mouseX<300 && mouseY>150 && mouseY<200) {
    fill(255, 0, 0);
    cursor(HAND);
  } else {
    fill(128, 0, 0);
    cursor(ARROW);
  }
  noStroke();
  rect(100, 150, 200, 50);
}

void mousePressed() {
  if (mouseX>100 && mouseX<300 && mouseY>150 && mouseY<200) {
    backgroundIsBlack = !backgroundIsBlack;
  }
}
