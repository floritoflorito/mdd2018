float x= 310;
float y= 200;
float w= 10;
float h= 20;
float r=255;
float g=40;
float a=40;

void setup() {
  size(600, 600);
  background(0);
}

void draw() {

  stroke(r, g, 10, a);
  r=r*0.99;
  g = g+1;
  a=a*0.995;
  //fill(0, 255, 0,32);
  noFill();
  rect(x, y, w, h);
  x=x-1.1;
  y=y-0.2;
  w=w+1;
  h=h+1.5;
}
