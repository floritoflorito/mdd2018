// draw a different cross
float horizontalOffset = 1;

// LET' MAKE A CANVAS
void setup() {
  // here! everything that runs once
  size(999, 333);
  // let' choose a different background color or even a photo
  background(232, 178, 220);
  // choose a color
  stroke(48, 60, 178);
  // and ends here.
}

void draw() {
  // this runs on every frame
  background(232, 178, 220);
  line(90+horizontalOffset, 10, 20+horizontalOffset, 80);
  line(20+horizontalOffset, 20, 80+horizontalOffset, 80);
  horizontalOffset = horizontalOffset+02;
}
