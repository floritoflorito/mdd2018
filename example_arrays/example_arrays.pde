int amount = 100;
float[] positions; // an array of positions
float[] velocities; // an array of velocities

void setup() {
  size(600,300);
  
  // setup the arrays: (create two list' of each containing 100 variables)
  positions = new float[amount];
  velocities = new float[amount];
  
  // initialize the arrays with values. (at the moment everything is 0)
  
  // a loop that counts from 0 to amount-1:
  // to go through each single element of the arrays
  for (int i=0; i<amount; i=i+1) {
    // set the positions to a random value
    positions[i] = random(width);
    // set the velocities to some random values
    velocities[i] = random(-1,1);
  }
  
  // we now have a list of positions and a list of velocities;
  // they could be something like this:
  // positions: 0, 400, 221, 33.5, 27.8, 385, etc..
  // velocities: -0.8, 0.2, 0.99, 0.032, -0.001, -0.031, etc..
  
}


void draw() {
  background(255); // clear the screen
  stroke(0,64); // use a black pen with a bit of opacity
  strokeWeight(3); // make the lines a bit thicker
  
  // just like in setup:
  // a loop to go through each single element of the arrays
  for (int i=0; i<amount; i=i+1) {
    
    // let's draw vertical lines on the positions
    line(positions[i], 0, positions[i], height);
    
    // .. and let' change each position
    positions[i] = positions[i] + velocities[i];
  }
  
}
