/*
Let' draw a bunch of vertical lines
*/

void setup() {
  size(600,600);
}

void draw() {
  background(240,248,255);
  
  stroke(0);
  //line(10,0,10,600);
  //line(20,0,20,600);
  //line(30,0,30,600);
  
  for (int x=10;x<600;x=x+10) {
    line(x,0,x,600);
  }
}
